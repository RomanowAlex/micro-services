package ru.romanow.services.warehouse.exceptions;

public class WarrantyProcessException
        extends RuntimeException {
    public WarrantyProcessException(String message) {
        super(message);
    }
}
