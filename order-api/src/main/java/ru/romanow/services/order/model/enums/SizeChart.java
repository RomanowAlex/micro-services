package ru.romanow.services.order.model.enums;

public enum SizeChart {
    S, M, L, XL
}
