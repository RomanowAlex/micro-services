package ru.romanow.services.order.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.util.UUID;

@Data
@Accessors(chain = true)
public class CreateOrderResponse {
    private UUID orderId;
}
