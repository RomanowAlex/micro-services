package ru.romanow.services.order.model.enums;

public enum PaymentStatus {
    PAID, CANCELED, WAITING
}
