package ru.romanow.services.warranty.modal.enums;

public enum WarrantyDecision {
    RETURN, FIXING, REFUSE
}
