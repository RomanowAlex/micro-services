package ru.romanow.services.warehouse.model.enums;

public enum SizeChart {
    S, M, L, XL
}
