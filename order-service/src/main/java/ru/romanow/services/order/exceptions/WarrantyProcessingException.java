package ru.romanow.services.order.exceptions;

public class WarrantyProcessingException
        extends RuntimeException {
    public WarrantyProcessingException(String message) {
        super(message);
    }
}
