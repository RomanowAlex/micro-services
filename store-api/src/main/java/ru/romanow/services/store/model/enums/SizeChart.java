package ru.romanow.services.store.model.enums;

public enum SizeChart {
    S, M, L, XL
}
