package ru.romanow.services.store.model.enums;

public enum WarrantyStatus {
    ON_WARRANTY, USE_WARRANTY, REMOVED_FROM_WARRANTY
}
