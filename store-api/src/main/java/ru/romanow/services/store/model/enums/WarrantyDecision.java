package ru.romanow.services.store.model.enums;

public enum WarrantyDecision {
    RETURN, REFUSE
}
