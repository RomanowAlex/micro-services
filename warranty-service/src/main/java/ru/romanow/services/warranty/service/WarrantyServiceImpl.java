package ru.romanow.services.warranty.service;

import lombok.AllArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.romanow.services.warranty.domain.Warranty;
import ru.romanow.services.warranty.modal.ItemWarrantyRequest;
import ru.romanow.services.warranty.modal.OrderWarrantyResponse;
import ru.romanow.services.warranty.modal.WarrantyInfoResponse;
import ru.romanow.services.warranty.modal.enums.WarrantyDecision;
import ru.romanow.services.warranty.modal.enums.WarrantyStatus;
import ru.romanow.services.warranty.repository.WarrantyRepository;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import javax.persistence.EntityNotFoundException;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.UUID;

import static java.time.LocalDateTime.now;
import static java.time.LocalDateTime.ofInstant;
import static java.time.ZoneId.systemDefault;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;

@Service
@AllArgsConstructor
public class WarrantyServiceImpl
        implements WarrantyService {
    private static final Logger logger = LoggerFactory.getLogger(WarrantyService.class);

    private final WarrantyRepository warrantyRepository;

    @Nonnull
    @Override
    @Transactional(readOnly = true)
    public Warranty getWarrantyByItemId(@Nonnull UUID itemId) {
        return warrantyRepository
                .findWarrantyByItemId(itemId)
                .orElseThrow(() -> new EntityNotFoundException("Warranty not found for itemId '" + itemId + "'"));
    }

    @Nullable
    @Override
    @Transactional(readOnly = true)
    public WarrantyInfoResponse getWarrantyInfo(@Nonnull UUID itemId) {
        logger.info("Get warranty info fot item '{}'", itemId);
        return warrantyRepository
                .findWarrantyByItemId(itemId)
                .map(this::buildWarrantyInfo)
                .orElse(null);
    }

    @Nonnull
    @Override
    @Transactional
    public OrderWarrantyResponse warrantyRequest(@Nonnull UUID itemId, @Nonnull ItemWarrantyRequest request) {
        logger.info("Process warranty request (reason: {}) for item '{}'", request.getReason(), itemId);
        final Warranty warranty = getWarrantyByItemId(itemId);

        WarrantyDecision decision = WarrantyDecision.REFUSE;
        if (isActiveWarranty(warranty) && warranty.getStatus() == WarrantyStatus.ON_WARRANTY) {
            decision = request.getAvailableCount() > 0 ? WarrantyDecision.RETURN : WarrantyDecision.FIXING;
        }

        logger.info("Warranty decision on item '{}' is {} (count: {}, status: {})",
                    itemId, decision, request.getAvailableCount(), warranty.getStatus());

        updateWarranty(warranty, decision, request.getReason());

        return new OrderWarrantyResponse()
                .setDecision(decision)
                .setWarrantyDate(formatDate(warranty.getWarrantyDate()));
    }

    @Override
    @Transactional
    public void startWarranty(@Nonnull UUID itemId) {
        logger.info("Start warranty for item '{}'", itemId);

        Warranty warranty =
                new Warranty()
                        .setWarrantyDate(new Date())
                        .setItemId(itemId)
                        .setStatus(WarrantyStatus.ON_WARRANTY);

        warranty = warrantyRepository.save(warranty);
        if (logger.isDebugEnabled()) {
            logger.debug("Create warranty {} for itemId '{}'", warranty, itemId);
        }
    }

    @Override
    @Transactional
    public void stopWarranty(@Nonnull UUID itemId) {
        logger.info("Remove item '{}' from warranty", itemId);

        final int deleted = warrantyRepository.stopWarranty(itemId);
        if (logger.isDebugEnabled()) {
            logger.debug("Deleted {} warranty", deleted);
        }
    }

    private void updateWarranty(@Nonnull Warranty warranty, @Nonnull WarrantyDecision decision, @Nullable String reason) {
        warranty.setComment(reason);
        final WarrantyStatus status =
                decision == WarrantyDecision.REFUSE
                        ? WarrantyStatus.REMOVED_FROM_WARRANTY
                        : WarrantyStatus.USE_WARRANTY;

        if (logger.isDebugEnabled()) {
            logger.debug("Set warranty status {} on item '{}'", status, warranty.getItemId());
        }

        warranty.setStatus(status);
        warranty = warrantyRepository.save(warranty);

        if (logger.isDebugEnabled()) {
            logger.debug("Update warranty {} for itemId '{}'", warranty, warranty.getItemId());
        }
    }

    private boolean isActiveWarranty(@Nonnull Warranty warranty) {
        final LocalDateTime warrantyDate = ofInstant(warranty.getWarrantyDate().toInstant(), systemDefault());
        return warrantyDate.isAfter(now().minus(1, ChronoUnit.MONTHS));
    }

    @Nonnull
    private WarrantyInfoResponse buildWarrantyInfo(@Nonnull Warranty warranty) {
        return new WarrantyInfoResponse()
                .setItemId(warranty.getItemId())
                .setStatus(warranty.getStatus())
                .setWarrantyDate(formatDate(warranty.getWarrantyDate()));
    }

    @Nonnull
    private String formatDate(@Nonnull Date warrantyDate) {
        return ofInstant(warrantyDate.toInstant(), systemDefault()).format(ISO_DATE_TIME);
    }
}
